# Contributing to this project
## Issues
If you see a problem, or have a suggestion, but don't know how to fix/implement it, open an issue using the guidelines in the issue templates.
## Pull Requests
If you see a problem, or have a suggestion, and know how to fix/implement it, open a pull request.
## On Forks
I prefer you fork this only if you have your own modifications only for yourself, but it doesn't really matter if you do a fork, then open a pull request.
